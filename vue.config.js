module.exports = {
  lintOnSave: false,

  // development server configuration
  devServer: {
    clientLogLevel: 'warning',
    hot           : true,
    compress      : true,
    host          : process.env.HOST || 'localhost',
    port          : process.env.PORT && Number(process.env.PORT) || 8080,
    open          : false,
    overlay       : {warnings: false, errors: true},
    proxy         : {
      '/api'        : {
        target      : 'http://localhost:8181',
        changeOrigin: true,
        pathRewrite : {
          '^/api': '',
        },
      },
    },
    watchOptions  : {
      poll: false,
    },
  },

  css: {
    sourceMap: 'production' !== process.env.NODE_ENV,
  },
};
