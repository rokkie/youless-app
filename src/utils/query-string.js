import * as object from './object';

/**
 * Convert a query string to an object with key/values
 *
 * @param  {String} qs  The query string
 * @return {*}          An object with key/value pairs
 */
export const parse = qs => {
  return qs
    .split('&')
    .filter(val => '' !== val)
    .reduce((acc, kv) => {
      const pair = kv.split('='),
            key  = decodeURIComponent(pair[0]);

      acc[key] = (2 > pair.length) ? true : decodeURIComponent(pair[1]);
      return acc;
    }, {});
};

/**
 * Convert an object with key/values to a query string
 *
 * @param   {Object}  params  An object with key/value pairs
 * @returns {String}          The query string
 */
export const stringify = params => object.paths(params).map(path => {
  const val   = object.path(params, path.slice(0)),
        first = encodeURIComponent(path.shift()),
        parts = (0 < path.length) ? path.map(part => `[${encodeURIComponent(part)}]`) : [],
        key   = [first].concat(parts).join('');

  return (Array.isArray(val))
    ? val.map(v => [`${key}[]`, encodeURIComponent(v)].join('=')).join('&')
    : [key, encodeURIComponent(val)].join('=');
}).join('&');
