/**
 * Test if a value is an object
 *
 * @param   {*}       value             Value to test
 * @param   {Boolean} [literal = false] Test for object literal; Defaults to FALSE
 * @returns {Boolean}                   If the value is object
 */
export const is = (value, literal = false) => (literal)
  ? (value instanceof Object && Object === value.constructor)
  : ('[object Object]' === Object.prototype.toString.call(value));

/**
 * Get value of an object at a given path
 *
 * @param  {Object} obj Object to get the path from
 * @param  {Array}  p   Path of the property
 * @return {*}          The value at the given path
 */
export const path = (obj, p) => {
  const key = p.shift(),
        val = obj[key];

  return (0 < p.length) ? path(val, p) : val;
};

/**
 * Get the paths of an object as arrays
 *
 * @param  {Object} obj           Object to get the paths from
 * @param  {Array}  [parent = []] Paths of parent object
 * @return {Array}                An array of property paths
 */
export const paths = (obj, parent = []) => Object
  .keys(obj)
  .reduce((acc, key) => {
    const val  = obj[key],
          path = (is(val)) ? paths(val, parent.concat(key)) : [parent.concat(key)];

    return acc.concat(path);
  }, []);
