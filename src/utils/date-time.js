
const RE_DATE = /^(?<year>[0-9]{4})-(?<month>[0-9]{2})-(?<day>[0-9]{2})$/;
const RE_TIME = /^(?<hour>[0-9]{2}):(?<min>[0-9]{2})(?::(?<sec>[0-9]{2}))?$/;


/**
 * Construct Date from separate date/time strings
 *
 * @param  {string} date  YYYY-MM-dd
 * @param  {string} time  hh:mm:ss
 * @return {Date|null}
 */
const fromDateTime = (date, time) => {
  if (!RE_DATE.test(date) || !RE_TIME.test(time)) { return null; }

  const {groups: {day, month, year}} = RE_DATE.exec(date);
  const {groups: {hour, min, sec}}   = RE_TIME.exec(time);

  return new Date(
    Number.parseInt(year, 10),
    Number.parseInt(month) - 1,
    Number.parseInt(day, 10),
    Number.parseInt(hour, 10),
    Number.parseInt(min, 10),
    sec ? Number.parseInt(sec, 10) : 0,
    0
  );
};

export {
  fromDateTime,
}
