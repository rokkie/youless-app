/**
 * @typedef  {Object} SelectOption
 * @property {string} label Text to show in de UI
 * @property {string} value Underlying value
 */

/**
 * @typedef  {Object}      ApiParams
 * @property {string}      type       gas|electricity
 * @property {granularity} type       min|hour
 * @property {number}      dateStart  Start date/time in seconds since epoch
 * @property {number}      dateEnd    End date/time in seconds since epoch
 */

/**
 * @typedef  {Object}  UsageChartData
 * @property {string}  type           gas|electricity
 * @property {string}  granularity    min|hour
 * @property {boolean} dateStartShow  If datepicker for start date should be shown
 * @property {string}  dateStart      Start date
 * @property {boolean} timeStartShow  If timepicker for start time should be shown
 * @property {string}  timeStart      Start time
 * @property {boolean} dateEndShow    If datepicker for end date should be shown
 * @property {string}  dateEnd        End date
 * @property {boolean} timeEndShow    If timepicker for end time should be shown
 * @property {string}  timeEnd        End time
 * @property {boolean} loaded         If data is loaded
 * @property {object}  chartData      Current chart data
 * @property {object}  chartOptions   Chart options
 */
